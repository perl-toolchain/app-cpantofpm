#!/usr/bin/perl

# [[[ HEADER ]]]
use strict;
use warnings;
#use RPerl::AfterSubclass;  # disable RPerl itself
our $VERSION = 0.001_000;

# disable RPerl itself
package string;
package string_arrayref;
package string_hashref;
package integer;
package main;

# [[[ CRITICS ]]]
## no critic qw(ProhibitUselessNoCritic ProhibitMagicNumbers RequireCheckedSyscalls)  # USER DEFAULT 1: allow numeric values & print operator
## no critic qw(RequireInterpolationOfMetachars)  # USER DEFAULT 2: allow single-quoted control characters & sigils

# [[[ INCLUDES ]]]
use English;
use IPC::Run3 qw(run3);
use JSON;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

# [[[ OPERATIONS ]]]

my string $dist_input = 'RPerl';

# DISTRIBUTION, FOR PROVIDES

# HARD-CODED EXAMPLE:
# curl https://fastapi.metacpan.org/v1/release/RPerl
my string $execute_command_dist = 'curl https://fastapi.metacpan.org/v1/release/' . $dist_input;

my string $json_dist = run_metacpan_query($execute_command_dist, $dist_input);
#print 'have $json_dist = ' . "\n" . $json_dist . "\n";

my string_hashref $json_dist_parsed = decode_json $json_dist;
#print 'have $json_dist_parsed = ' . "\n" . Dumper($json_dist_parsed) . "\n";
#print 'have $json_dist_parsed->{provides} = ' . "\n" . Dumper($json_dist_parsed->{provides}) . "\n";

# convert provides from array to hash for quick lookup during iteration
my string_hashref $dist_provides = {};
foreach my string $dist_provide (@{$json_dist_parsed->{provides}}) {
    $dist_provides->{$dist_provide} = undef;
}
#print 'have $dist_provides = ' . "\n" . Dumper($dist_provides) . "\n";


# MODULES, FOR VERSIONS

# call metacpan API to find all modules belonging to distribution
my string $execute_command_modules =<<EOL;
curl -XPOST https://fastapi.metacpan.org/v1/module/_search -d '
{
    "query" : {
        "constant_score" : {
            "filter" : {
                "exists" : { "field" : "module" }
            }
        }
    },
    "size": 5000,
    "_source": [ "name", "module.name", "module.version" ],
    "filter": {
        "and": [
            { "term": { "distribution": "__INSERT_DISTRIBUTION__" } },
            { "term": { "maturity": "released" } },
            { "term": { "status": "latest" } }
        ]
    }
}'
EOL
$execute_command_modules =~ s/__INSERT_DISTRIBUTION__/$dist_input/gxms;

my string $json_modules = run_metacpan_query($execute_command_modules, $dist_input);
#print 'have $json_modules = ' . "\n" . $json_modules . "\n";

my string_hashref $json_modules_parsed = decode_json $json_modules;
#print 'have $json_modules_parsed = ' . "\n" . Dumper($json_modules_parsed) . "\n";
#print 'have $json_modules_parsed->{hits}->{hits} = ' . "\n" . Dumper($json_modules_parsed->{hits}->{hits}) . "\n";


# FIND VERSIONS

# loop through all modules belonging to distribution, find those present in provides
foreach my string_hashref $module (@{$json_modules_parsed->{hits}->{hits}}) {
    foreach my string_hashref $package (@{$module->{_source}->{module}}) {
        if (exists $dist_provides->{$package->{name}}) {
            if ((exists $package->{version}) and (defined $package->{version})) {
                $dist_provides->{$package->{name}} = $package->{version};
            }
            else {
                $dist_provides->{$package->{name}} = -1;
            }
        }
    }
}

print 'have versioned $dist_provides = ' . "\n" . Dumper($dist_provides) . "\n";


# START HERE: copy logic & process to ruby
# START HERE: copy logic & process to ruby
# START HERE: copy logic & process to ruby


# [[[ SUBROUTINES ]]]
    
sub run_metacpan_query {
    { my string_arrayref $RETURN_TYPE };
    ( my string $execute_command, my string $dist_input ) = @ARG;

    my string $stdout_generated = q{};
    my string $stderr_generated = q{};

#print {*STDERR} 'in run_metacpan_query(), have $execute_command = ' . $execute_command . "\n";
#print {*STDERR} 'in run_metacpan_query(), about to call run3()...' . "\n";

    run3( $execute_command, \undef, \$stdout_generated, \$stderr_generated );  # no simultaneous view & capture; child STDIN from /dev/null, child STDOUT & STDERR to variables

#print {*STDERR} 'in run_metacpan_query(), have $stdout_generated = ', "\n", '<<<=== BEGIN STDOUT ===>>>', $stdout_generated, '<<<=== END STDOUT ===>>>', "\n\n";
#print {*STDERR} 'in run_metacpan_query(), have $stderr_generated = ', "\n", '<<<=== BEGIN STDERR ===>>>', $stderr_generated, '<<<=== END STDERR ===>>>', "\n\n";

    my integer $test_exit_status = $CHILD_ERROR >> 8;

    if ( $test_exit_status == 0 ) {    # UNIX process return code 0, success
        # DEV NOTE: must print to STDERR in order to avoid delayed output
#        print {*STDERR} "\n", ('[[[[[=====  METACPAN API CALL SUCCESS!  ', $dist_input, '  =====]]]]]' . "\n") x 3, "\n";
    }
    elsif ($stderr_generated ne q{}) {
        die "\n", ('[[[[[==== METACPAN API CALL ERROR!  ', $dist_input, '  =====]]]]]' . "\n") x 3, $stderr_generated, "\n", 'dying', "\n";
    }
    else {    # UNIX process return code not 0, error
        die "\n", ('[[[[[==== METACPAN API CALL ERROR!  ', $dist_input, '  =====]]]]]' . "\n") x 3, 'dying', "\n";
    }

    return $stdout_generated;
}



